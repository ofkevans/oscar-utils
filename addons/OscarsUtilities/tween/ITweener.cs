namespace OscarsUtilities.Tweening
{
    public interface ITweener
    {
        void Start(Godot.Tween tween);
    }
}