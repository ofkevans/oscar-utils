using Godot;
using System;
using System.Linq;
using System.Collections.Generic;

namespace OscarsUtilities.Tweening
{
    public class TweenSequence : Object, ITweener
    {
        public List<List<ITweener>> Tweeners { get; set; }

        private SceneTree Tree;
        private Tween Tween;

        private int CurrentStep = 0;
        private int Loops = 0;
        private bool AutoStart = true;
        private bool Started = false;
        private bool Running = false;

        private bool KillWhenFinished = true;
        private bool Parallel = false;

        public TweenSequence(SceneTree tree) : base()
        {
            Tree = tree;
            Tweeners = new List<List<ITweener>>();
        }

        public void Start(Tween tween)
        {
            Tween = tween;
        }

        public void Start()
        {
        }

        public TweenSequence WithLoops(int loops = -1)
        {
            Loops = loops;
            return this;
        }
        
        // Pass most recent tween to a configuration lambda and replace with results
        public TweenSequence WithOptions<T>(Func<T, T> configurator) where T: struct, ITweener
        {
            var lastTween = Tweeners.Last().Last();
            Tweeners.Last().RemoveAt(Tweeners.Last().Count - 1);
            Tweeners.Last().Append(configurator(lastTween as T));
            return this;
        }
        
        public TweenSequence WithOptions<T>(Func<T, T> configurator) where T: class, ITweener
        {
            
        }

        public TweenSequence AddTweener(ITweener tweener)
        {
            if (!Parallel)
            {
                Tweeners.Add(new List<ITweener>());
            }
            Parallel = false;
            return this;
        }
    }
}