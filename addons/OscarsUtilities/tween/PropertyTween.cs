using Godot;

namespace OscarsUtilities.Tweening
{
    public struct PropertyTween : ITweener
    {
        private Object Target;
        private NodePath Property;
        private object From;
        private object To;
        private float Duration;
        private Tween.TransitionType Trans;
        private Tween.EaseType Ease;

        private float Delay;
        private bool Continue;
        private bool Advance;

        public PropertyTween(Object target, NodePath property, object to, float duration)
        {
            Target = target;
            Property = property;

            From = Target.GetIndexed(Property);
            To = to;

            Duration = duration;

            Trans = Tween.TransitionType.Linear;
            Ease = Tween.EaseType.InOut;

            Delay = 0.0f;
            Continue = true;
            Advance = false;
        }

        public PropertyTween FromValue(object value)
        {
            From = value;
            Continue = false;
            return this;
        }

        public PropertyTween FromCurrent()
        {
            Continue = false;
            return this;
        }

        public PropertyTween WithTransition(Tween.TransitionType transition)
        {
            Trans = transition;
            return this;
        }

        public PropertyTween WithEase(Tween.EaseType ease)
        {
            Ease = ease;
            return this;
        }

        public PropertyTween WithDelay(float delay)
        {
            Delay = delay;
            return this;
        }

        public void Start(Tween tween)
        {
            if (!Object.IsInstanceValid(Target))
            {
                GD.PushWarning("Target object freed, aborting tween");
                return;
            }

            if (Continue) From = Target.GetIndexed(Property);
            if (Advance)
            {
                var to = From.GetType().GetMethod("op_Addition").Invoke(null, new object[] { From, To });
                tween.InterpolateProperty(Target, Property, From, to, Duration, Trans, Ease, Delay);
            }
            else
            {
                tween.InterpolateProperty(Target, Property, From, To, Duration, Trans, Ease, Delay);
            }
        }
    }
}